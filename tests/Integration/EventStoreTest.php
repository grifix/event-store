<?php

/** @noinspection SqlResolve */
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\BigInt\BigInt;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\EventType\Repository\Exceptions\EventTypeAlreadyExistsException;
use Grifix\EventStore\EventType\Repository\Exceptions\EventTypeDoesNotExistException;
use Grifix\EventStore\StreamType\Repository\Exceptions\StreamTypeAlreadyExistsException;
use Grifix\EventStore\StreamType\Repository\Exceptions\StreamTypeDoesNotExist;
use Grifix\EventStore\Subscription\Exceptions\CannotPauseSubscriptionException;
use Grifix\EventStore\Subscription\SubscriptionDto;
use Grifix\EventStore\SubscriptionType\Repository\Exceptions\SubscriptionTypeAlreadyExistsException;
use Grifix\EventStore\Tests\Integration\Dummies\Domain\DomainException;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Client\Client;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Client\Events\ClientCreatedEvent;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Client\Events\ClientDeletedEvent;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Client\Events\ClientUpdatedEvent;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Order\Events\OrderCompletedEvent;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Order\Events\OrderCompletedVersionConverter;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Order\Order;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Unknown\Events\UnregisteredEvent;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Unknown\Unknown;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\User\Events\UserCreatedEvent;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\User\Events\UserDeletedEvent;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\User\Events\UserUpdatedEvent;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\User\User;
use Grifix\EventStore\Tests\Integration\Dummies\Projector\Projector;
use Grifix\EventStore\Tests\Integration\Dummies\Projector\RecordDto;
use Grifix\EventStore\Tests\Integration\Dummies\Repository\Repository;
use Grifix\EventStore\Tests\Integration\Dummies\Subscriber;
use Grifix\EventStore\Workers\Exceptions\InvalidEventClassException;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Uuid\Uuid;
use Monolog\Logger;

final class EventStoreTest extends IntegrationTest
{
    private Repository $repository;

    private Projector $projector;

    private readonly Subscriber $subscriber;

    private const SUBSCRIPTION_TYPE = 'test_subscription';

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = new Repository($this->connection);
        $this->projector = new Projector($this->connection);
        $this->subscriber = new Subscriber();

        $this->registerStreamsAndEvents();
        $this->registerSubscription();
        $this->eventStore->registerSubscriber($this->subscriber);

        $this->messageBroker->startConsumer(function (EventEnvelope $envelope) {
            $this->eventStore->processEvent($envelope);
        });
    }

    private function registerStreamsAndEvents(): void
    {
        $this->eventStore->registerStreamType('user', User::class);
        $this->eventStore->registerEventType(
            'user',
            'created',
            UserCreatedEvent::class,
            [
                Schema::create()
                    ->withStringProperty('userId')
                    ->withStringProperty('email')
            ],
        );
        $this->eventStore->registerEventType(
            'user',
            'updated',
            UserUpdatedEvent::class,
            [
                Schema::create()
                    ->withStringProperty('userId')
                    ->withStringProperty('newEmail')
            ],
        );
        $this->eventStore->registerEventType(
            'user',
            'deleted',
            UserDeletedEvent::class,
            [
                Schema::create()
                    ->withStringProperty('userId')
            ],
        );

        $this->eventStore->registerStreamType('client', Client::class);
        $this->eventStore->registerEventType(
            'client',
            'created',
            ClientCreatedEvent::class,
            [
                Schema::create()
                    ->withStringProperty('clientId')
                    ->withStringProperty('name')
            ],
        );
        $this->eventStore->registerEventType(
            'client',
            'updated',
            ClientUpdatedEvent::class,
            [
                Schema::create()
                    ->withStringProperty('clientId')
                    ->withStringProperty('newName')
            ]
        );
        $this->eventStore->registerEventType(
            'client',
            'deleted',
            ClientDeletedEvent::class,
            [
                Schema::create()
                    ->withStringProperty('clientId')
            ]
        );
    }

    public function registerSubscription(): void
    {
        $this->eventStore->registerSubscriptionType(
            self::SUBSCRIPTION_TYPE,
            Subscriber::class,
            'user',
            [
                'user.created',
            ],
            [
                'user.deleted',
            ]
        );
    }

    private function setUpSagaSubscriber(): void
    {
        $this->subscriber->setCallback(UserCreatedEvent::class, function (UserCreatedEvent $event) {
            $this->connection->beginTransaction();
            $client = new Client($event->userId, $event->email);
            $this->repository->add($client);
            $this->eventStore->storeEvent(
                new ClientCreatedEvent($client->getId(), $client->getName()),
                Client::class,
                Uuid::createFromString($client->getId())
            );
            $this->eventStore->flush();
            $this->connection->commit();
        });
        $this->subscriber->setCallback(UserUpdatedEvent::class, function (UserUpdatedEvent $event) {
            $this->connection->beginTransaction();
            $client = $this->repository->findClient($event->userId);
            $client->setName($event->newEmail);
            $this->eventStore->storeEvent(
                new ClientUpdatedEvent($client->getId(), $client->getName()),
                Client::class,
                Uuid::createFromString($client->getId())
            );
            $this->repository->update($client);
            $this->eventStore->flush();
            $this->connection->commit();
        });
        $this->subscriber->setCallback(UserDeletedEvent::class, function (UserDeletedEvent $event) {
            $this->connection->beginTransaction();
            $client = $this->repository->findClient($event->userId);
            $this->repository->delete($client->getId());
            $this->eventStore->storeEvent(
                new ClientDeletedEvent($client->getId()),
                Client::class,
                Uuid::createFromString($client->getId())
            );
            $this->eventStore->flush();
            $this->connection->commit();
        });
    }

    private function setUpProjectorSubscriber(): void
    {
        $this->subscriber->setCallback(
            UserCreatedEvent::class,
            function (UserCreatedEvent $event) {
                $this->projector->createRecord(new RecordDto($event->userId, $event::class, $event->email));
            }
        );

        $this->subscriber->setCallback(
            UserUpdatedEvent::class,
            function (UserUpdatedEvent $event) {
                $this->projector->createRecord(new RecordDto($event->userId, $event::class, $event->newEmail));
            }
        );

        $this->subscriber->setCallback(
            UserDeletedEvent::class,
            function (UserDeletedEvent $event) {
                $this->projector->createRecord(new RecordDto($event->userId, $event::class));
            }
        );
    }

    public function testItStoresEvents(): void
    {
        $this->clock->freezeTime(new \DateTimeImmutable('2020-01-01 00:00:00.000000'));
        $event = new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com');
        $this->eventStore->storeEvent(
            $event,
            User::class,
            Uuid::createFromString('8e9e733e-c3df-48d8-b1c1-86388967b335')
        );
        $this->eventStore->flush();

        $envelopes = array(...$this->eventStore->findEvents());
        /** @var EventEnvelope $envelope */
        $envelope = $envelopes[0];
        self::assertCount(1, $envelopes);
        self::assertEquals(Uuid::createFromString('8e9e733e-c3df-48d8-b1c1-86388967b335'), $envelope->streamId);
        self::assertEquals($event, $envelope->event);
        self::assertEquals(BigInt::create(1), $envelope->number);
        self::assertEquals('user', $envelope->streamTypeName);
        self::assertEquals('created', $envelope->eventTypeName);
        self::assertEquals(new \DateTimeImmutable('2020-01-01 00:00:00.000000'), $envelope->dateOfCreation);
        self::assertEquals(null, $envelope->dateOfPublishing);
    }

    public function testItCreatesSubscription(): void
    {
        $this->setUpSagaSubscriber();
        $this->publishEvent(
            new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );

        $this->loggerHandler->clear();
        $this->messageBroker->run();

        self::assertEquals(
            'select count(*) from grifix_event_store.subscriptions where stream_id = ? and type = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals('Beginning transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'INSERT INTO grifix_event_store.subscriptions (type, stream_id, last_received_event_number, status) VALUES (?, ?, ?, ?)',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals('Committing transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Beginning transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'INSERT INTO entities (id, type, value) VALUES (?, ?, ?)',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select max(number) as number from grifix_event_store.events where stream_id=? and stream_type_name=?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'INSERT INTO grifix_event_store.events (id, stream_type_name, stream_id, number, date_of_creation, event_type_name, event) VALUES (?, ?, ?, ?, ?, ?, ?)',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'UPDATE grifix_event_store.subscriptions SET stream_id = ?, last_received_event_number = ?, status = ? WHERE stream_id = ? AND type = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals('Committing transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Disconnecting', $this->loggerHandler->shiftRecord()['message']);

        /** @var EventEnvelope[] $events */
        $events = array(...$this->eventStore->findEvents());
        self::assertCount(2, $events);
        self::assertEquals('user.created', $events[0]->getEventType());
        self::assertEquals('client.created', $events[1]->getEventType());

        /** @var SubscriptionDto $subscription */
        $subscription = array(
            ...$this->eventStore->findSubscriptions(
                self::SUBSCRIPTION_TYPE,
                '8e9e733e-c3df-48d8-b1c1-86388967b335'
            )
        )[0];

        self::assertEquals('active', $subscription->status);
        self::assertEquals('1', $subscription->lastReceivedEventNumber->toString());
        self::assertEquals(
            new Client('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com'),
            $this->repository->findClient('8e9e733e-c3df-48d8-b1c1-86388967b335')
        );
    }

    public function testItUpdatesSubscription(): void
    {
        $this->setUpSagaSubscriber();
        $this->publishEvent(
            new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();

        $this->publishEvent(
            new UserUpdatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'new@test.com'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->loggerHandler->clear();
        $this->messageBroker->run();

        self::assertEquals('Disconnecting', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Connecting with parameters {params}', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals(
            'select count(*) from grifix_event_store.subscriptions where stream_id = ? and type = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals('Beginning transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select * from entities where id=? and type=? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select max(number) as number from grifix_event_store.events where stream_id=? and stream_type_name=?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'UPDATE entities SET type = ?, value = ? WHERE id = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'INSERT INTO grifix_event_store.events (id, stream_type_name, stream_id, number, date_of_creation, event_type_name, event) VALUES (?, ?, ?, ?, ?, ?, ?)',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'UPDATE grifix_event_store.subscriptions SET stream_id = ?, last_received_event_number = ?, status = ? WHERE stream_id = ? AND type = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals('Committing transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Disconnecting', $this->loggerHandler->shiftRecord()['message']);

        /** @var EventEnvelope[] $events */
        $events = array(...$this->eventStore->findEvents());
        self::assertCount(4, $events);
        self::assertEquals('client.updated', $events[3]->getEventType());

        /** @var SubscriptionDto $subscription */
        $subscription = array(
            ...$this->eventStore->findSubscriptions(
                self::SUBSCRIPTION_TYPE,
                '8e9e733e-c3df-48d8-b1c1-86388967b335'
            )
        )[0];

        self::assertEquals('active', $subscription->status);
        self::assertEquals('2', $subscription->lastReceivedEventNumber->toString());
        self::assertEquals(
            new Client('8e9e733e-c3df-48d8-b1c1-86388967b335', 'new@test.com'),
            $this->repository->findClient('8e9e733e-c3df-48d8-b1c1-86388967b335')
        );
    }

    public function testItFinishesSubscription(): void
    {
        $this->setUpSagaSubscriber();
        $this->publishEvent(
            new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();

        $this->publishEvent(
            new UserUpdatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'new@test.com'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();

        $this->publishEvent(
            new UserDeletedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );

        $this->loggerHandler->clear();
        $this->messageBroker->run();

        self::assertEquals('Disconnecting', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Connecting with parameters {params}', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals(
            'select count(*) from grifix_event_store.subscriptions where stream_id = ? and type = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals('Beginning transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select * from entities where id=? and type=? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'DELETE FROM entities WHERE id = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select max(number) as number from grifix_event_store.events where stream_id=? and stream_type_name=?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'INSERT INTO grifix_event_store.events (id, stream_type_name, stream_id, number, date_of_creation, event_type_name, event) VALUES (?, ?, ?, ?, ?, ?, ?)',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'UPDATE grifix_event_store.subscriptions SET stream_id = ?, last_received_event_number = ?, status = ? WHERE stream_id = ? AND type = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals('Committing transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Disconnecting', $this->loggerHandler->shiftRecord()['message']);

        /** @var EventEnvelope[] $events */
        $events = array(...$this->eventStore->findEvents());
        self::assertCount(6, $events);
        self::assertEquals('client.deleted', $events[5]->getEventType());

        /** @var SubscriptionDto $subscription */
        $subscription = array(
            ...$this->eventStore->findSubscriptions(
                self::SUBSCRIPTION_TYPE,
                '8e9e733e-c3df-48d8-b1c1-86388967b335'
            )
        )[0];
        self::assertEquals('finished', $subscription->status);
        self::assertEquals('3', $subscription->lastReceivedEventNumber->toString());
        self::assertEquals(
            null,
            $this->repository->findClient('8e9e733e-c3df-48d8-b1c1-86388967b335')
        );
    }

    public function testItCreatesSubscriptionOnSqlError(): void
    {
        $this->subscriber->setCallback(UserCreatedEvent::class, function (UserCreatedEvent $event) {
            $this->connection->update('not_existing_table', ['name' => 'joe'], ['id' => 1]);
        });

        $event = new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com');
        $this->publishEvent(
            $event,
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();

        $error = $this->loggerHandler->getRecords()[25];
        self::assertTrue(str_starts_with($error['message'], 'An exception occurred while executing a query:'));
        self::assertEquals(Logger::CRITICAL, $error['level']);
        self::assertEquals('Rolling back transaction', $this->loggerHandler->getRecords()[26]['message']);

        /** @var EventEnvelope[] $events */
        $events = array(...$this->eventStore->findEvents());
        self::assertCount(1, $events);
        self::assertEquals($event, $events[0]->event);
        self::assertCount(1, $this->eventStore->findSubscriptions());
        /** @var SubscriptionDto $subscription */
        $subscription = array(...$this->eventStore->findSubscriptions())[0];
        self::assertEquals('0', $subscription->lastReceivedEventNumber->toString());
    }

    public function testItHandlesCriticalError(): void
    {
        $this->subscriber->setCallback(UserCreatedEvent::class, function (UserCreatedEvent $event) {
            throw new \RuntimeException('Critical error');
        });

        $event = new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com');
        $this->publishEvent(
            $event,
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();


        $error = $this->loggerHandler->getRecords()[24];
        self::assertEquals('Critical error', $error['message']);
        self::assertEquals(Logger::CRITICAL, $error['level']);
        self::assertEquals('Rolling back transaction', $this->loggerHandler->getRecords()[25]['message']);

        /** @var EventEnvelope[] $events */
        $events = array(...$this->eventStore->findEvents());
        self::assertCount(1, $events);
        self::assertEquals($event, $events[0]->event);
        self::assertCount(1, $this->eventStore->findSubscriptions());
        /** @var SubscriptionDto $subscription */
        $subscription = array(...$this->eventStore->findSubscriptions())[0];
        self::assertEquals('0', $subscription->lastReceivedEventNumber->toString());
    }

    public function testItHandlesNotCriticalError(): void
    {
        $this->subscriber->setCallback(UserCreatedEvent::class, function (UserCreatedEvent $event) {
            throw new DomainException('Domain error');
        });

        $event = new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com');
        $this->publishEvent(
            $event,
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();


        $error = $this->loggerHandler->getRecords()[24];
        self::assertEquals('Domain error', $error['message']);
        self::assertEquals(Logger::ERROR, $error['level']);
        self::assertEquals('Committing transaction', $this->loggerHandler->getRecords()[25]['message']);

        /** @var EventEnvelope[] $events */
        $events = array(...$this->eventStore->findEvents());
        self::assertCount(1, $events);
        self::assertEquals($event, $events[0]->event);
        self::assertCount(1, $this->eventStore->findSubscriptions());
        /** @var SubscriptionDto $subscription */
        $subscription = array(...$this->eventStore->findSubscriptions())[0];
        self::assertEquals('0', $subscription->lastReceivedEventNumber->toString());
    }

    public function testItProcessesMissedEvents(): void
    {
        $this->setUpProjectorSubscriber();
        $events = [
            new UserCreatedEvent('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e', 'test@grifix.net'),
            new UserUpdatedEvent('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e', 'test1@grifix.net'),
            new UserUpdatedEvent('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e', 'test2@grifix.net'),
            new UserUpdatedEvent('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e', 'test3@grifix.net'),
            new UserDeletedEvent('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e')
        ];
        foreach ($events as $event) {
            $this->eventStore->storeEvent(
                $event,
                User::class,
                Uuid::createFromString('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e')
            );
        }

        $this->eventStore->flush();

        $envelopes = ArrayWrapper::create(array(...$this->eventStore->findEvents()));
        $this->messageBroker->send($envelopes->getFirstElement());
        $this->messageBroker->send($envelopes->getLastElement());

        $this->loggerHandler->clear();
        $this->messageBroker->run();

        /** @var SubscriptionDto $subscription */
        $subscription = array(
            ...$this->eventStore->findSubscriptions(
                self::SUBSCRIPTION_TYPE,
                '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e'
            )
        )[0];
        self::assertEquals('finished', $subscription->status);
        $records = $this->projector->getAllRecords();
        self::assertCount(5, $records);
        self::assertEquals(
            [
                new RecordDto(
                    '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e',
                    UserCreatedEvent::class,
                    'test@grifix.net',
                    1
                ),
                new RecordDto(
                    '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e',
                    UserUpdatedEvent::class,
                    'test1@grifix.net',
                    2
                ),
                new RecordDto(
                    '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e',
                    UserUpdatedEvent::class,
                    'test2@grifix.net',
                    3
                ),
                new RecordDto(
                    '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e',
                    UserUpdatedEvent::class,
                    'test3@grifix.net',
                    4
                ),
                new RecordDto(
                    streamId: '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e',
                    eventClass: UserDeletedEvent::class,
                    id: 5
                )
            ],
            $records
        );
    }

    public function testFinishedSubscriptionDoesNotProcessEvents(): void
    {
        $this->setUpProjectorSubscriber();
        $this->publishEvent(
            new UserCreatedEvent(
                'b41ae221-8fcf-4fac-8811-86c2778b59e1',
                'old_email'
            ),
            User::class,
            'b41ae221-8fcf-4fac-8811-86c2778b59e1'
        );
        $this->publishEvent(
            new UserDeletedEvent(
                'b41ae221-8fcf-4fac-8811-86c2778b59e1',
            ),
            User::class,
            'b41ae221-8fcf-4fac-8811-86c2778b59e1'
        );

        $this->publishEvent(
            new UserUpdatedEvent(
                'b41ae221-8fcf-4fac-8811-86c2778b59e1',
                'new_email'
            ),
            User::class,
            'b41ae221-8fcf-4fac-8811-86c2778b59e1'
        );

        $this->messageBroker->run();

        /** @var SubscriptionDto $subscription */
        $subscription = array(
            ...$this->eventStore->findSubscriptions(
                self::SUBSCRIPTION_TYPE,
                'b41ae221-8fcf-4fac-8811-86c2778b59e1'
            )
        )[0];

        self::assertEquals('2', $subscription->lastReceivedEventNumber->toString());
        $rows = $this->projector->getAllRecords();
        self::assertCount(2, $rows);
        self::assertEquals(
            [
                new RecordDto(
                    'b41ae221-8fcf-4fac-8811-86c2778b59e1',
                    UserCreatedEvent::class,
                    'old_email',
                    1
                ),
                new RecordDto(
                    'b41ae221-8fcf-4fac-8811-86c2778b59e1',
                    UserDeletedEvent::class,
                    id: 2
                ),
            ],
            $rows
        );
    }

    public function testItPausesSubscription(): void
    {
        $this->publishEvent(
            new UserCreatedEvent(
                'b74e494a-e927-44dd-8831-efca79c0b877',
                'test'
            ),
            User::class,
            'b74e494a-e927-44dd-8831-efca79c0b877'
        );
        $this->messageBroker->run();
        $this->eventStore->pauseSubscription(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        $this->eventStore->flush();


        /** @var SubscriptionDto $subscription */
        $subscription = array(
            ...$this->eventStore->findSubscriptions(
                self::SUBSCRIPTION_TYPE,
                'b74e494a-e927-44dd-8831-efca79c0b877'
            )
        )[0];

        self::assertEquals('paused', $subscription->status);
    }

    public function testItUnpausesSubscription(): void
    {
        $this->publishEvent(
            new UserCreatedEvent(
                'b74e494a-e927-44dd-8831-efca79c0b877',
                'test'
            ),
            User::class,
            'b74e494a-e927-44dd-8831-efca79c0b877'
        );
        $this->messageBroker->run();
        $this->eventStore->pauseSubscription(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        $this->eventStore->flush();

        $this->eventStore->unpauseSubscription(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        $this->eventStore->flush();

        /** @var SubscriptionDto $subscription */
        $subscription = array(
            ...$this->eventStore->findSubscriptions(
                self::SUBSCRIPTION_TYPE,
                'b74e494a-e927-44dd-8831-efca79c0b877'
            )
        )[0];

        self::assertEquals('active', $subscription->status);
    }

    public function testPausedSubscriptionDoesNotProcessEvents(): void
    {
        $this->publishEvent(
            new UserCreatedEvent(
                'b74e494a-e927-44dd-8831-efca79c0b877',
                'test'
            ),
            User::class,
            'b74e494a-e927-44dd-8831-efca79c0b877'
        );
        $this->messageBroker->run();

        $this->eventStore->pauseSubscription(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        $this->eventStore->flush();

        $this->publishEvent(
            new UserUpdatedEvent(
                'b74e494a-e927-44dd-8831-efca79c0b877',
                'test2'
            ),
            User::class,
            'b74e494a-e927-44dd-8831-efca79c0b877'
        );
        $this->messageBroker->run();

        /** @var SubscriptionDto $subscription */
        $subscription = array(
            ...$this->eventStore->findSubscriptions(
                self::SUBSCRIPTION_TYPE,
                'b74e494a-e927-44dd-8831-efca79c0b877'
            )
        )[0];

        self::assertEquals('1', $subscription->lastReceivedEventNumber->toString());
    }

    public function testSubscriptionDoesNotProcessEventsFromTheWrongStream(): void
    {
        $streamA = '43ea45a3-0b51-4450-8332-790720abbed8';
        $streamB = 'ac4305ab-3a27-4523-a493-f6f1a7d126ed';
        $this->publishEvent(
            new UserCreatedEvent(
                $streamA,
                'email'
            ),
            User::class,
            $streamA
        );

        $this->publishEvent(
            new UserUpdatedEvent(
                $streamA,
                'new_email'
            ),
            User::class,
            $streamA
        );

        $this->publishEvent(
            new UserCreatedEvent(
                $streamB,
                'email2'
            ),
            User::class,
            $streamB
        );

        $this->messageBroker->run();

        /** @var SubscriptionDto $subscriptionA */
        $subscriptionA = array(
            ...$this->eventStore->findSubscriptions(
                self::SUBSCRIPTION_TYPE,
                $streamA
            )
        )[0];

        /** @var SubscriptionDto $subscriptionB */
        $subscriptionB = array(
            ...$this->eventStore->findSubscriptions(
                self::SUBSCRIPTION_TYPE,
                $streamB
            )
        )[0];

        self::assertEquals('2', $subscriptionA->lastReceivedEventNumber->toString());
        self::assertEquals('1', $subscriptionB->lastReceivedEventNumber->toString());
    }

    public function testSubscriptionDoesNotCreateOnNonStartingEvent(): void
    {
        $this->publishEvent(
            new UserUpdatedEvent(
                'd6ef55d3-b89c-4120-8cf8-e3ba52e17753',
                'new_email'
            ),
            User::class,
            'd6ef55d3-b89c-4120-8cf8-e3ba52e17753'
        );
        $this->messageBroker->run();
        self::assertCount(0, $this->eventStore->findSubscriptions());
    }

    public function testItPublishesEvent(): void
    {
        $event = new UserCreatedEvent(
            'd6ef55d3-b89c-4120-8cf8-e3ba52e17753',
            'new_email'
        );
        $this->publishEvent(
            $event,
            User::class,
            'd6ef55d3-b89c-4120-8cf8-e3ba52e17753'
        );
        /** @var EventEnvelope[] $events */
        $events = array(...$this->eventStore->findEvents(published: true));
        self::assertCount(1, $events);
        self::assertEquals($event, $events[0]->event);
    }

    /**
     * @dataProvider itDoesNotRegisterEventTypeTwiceDataProvider
     */
    public function testItDoesNotRegisterEventTypeTwice(
        array $eventType,
        string $expectedException,
        string $expectedMessage
    ): void {
        $this->expectException($expectedException);
        $this->expectExceptionMessage($expectedMessage);
        $this->eventStore->registerEventType(
            $eventType['stream'],
            $eventType['name'],
            $eventType['class'],
            [
                Schema::create()
            ]
        );
    }

    public function itDoesNotRegisterEventTypeTwiceDataProvider(): array
    {
        return [
            [
                [
                    'stream' => 'user',
                    'name' => 'created',
                    'class' => UserCreatedEvent::class
                ],
                EventTypeAlreadyExistsException::class,
                'Event type with name "user.created" already exists!'
            ],
            [
                [
                    'stream' => 'order',
                    'name' => 'completed',
                    'class' => UserCreatedEvent::class
                ],
                EventTypeAlreadyExistsException::class,
                'Event type with event class "Grifix\EventStore\Tests\Integration\Dummies\EventProducers\User\Events\UserCreatedEvent" already exists!'
            ]
        ];
    }

    public function testItConvertsEvent(): void
    {
        $this->eventStore->registerVersionConverter(new OrderCompletedVersionConverter());
        $this->eventStore->registerStreamType('order', Order::class);
        $this->eventStore->registerEventType(
            'order',
            'completed',
            OrderCompletedEvent::class,
            [
                Schema::create()
                    ->withStringProperty('orderId'),
                Schema::create()
                    ->withStringProperty('orderId')
                    ->withStringProperty('comment')
            ],
            OrderCompletedVersionConverter::class
        );

        $event = new OrderCompletedEvent(
            'bf1d476d-cfc8-4d65-9551-4fba3115d6de',
            ''
        );
        $this->eventStore->storeEvent(
            $event,
            Order::class,
            Uuid::createFromString('bf1d476d-cfc8-4d65-9551-4fba3115d6de')
        );
        $this->eventStore->flush();
        $this->connection->update(
            'grifix_event_store.events',
            [
                'event' => json_encode([
                    'orderId' => 'bf1d476d-cfc8-4d65-9551-4fba3115d6de',
                    '__normalizer__' => [
                        'version' => 1,
                        'name' => 'order.completed'
                    ]
                ])
            ],
            ['number' => 1]
        );
        /** @var EventEnvelope[] $events */
        $events = array(...$this->eventStore->findEvents());
        self::assertCount(1, $events);
        self::assertEquals($event, $events[0]->event);
    }

    public function testItDoesNotStoreUnregisteredEvents(): void
    {
        $this->expectException(EventTypeDoesNotExistException::class);
        $this->expectExceptionMessage(
            'Event type for event class [Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Unknown\Events\UnregisteredEvent] does not exist!'
        );
        $this->eventStore->storeEvent(
            new UnregisteredEvent(),
            User::class,
            Uuid::createFromString('1dbda280-fa35-40d5-90e5-abf5619c614a')
        );
    }

    public function testItDoesNotProcessUnsupportedEvent(): void
    {
        $this->expectException(InvalidEventClassException::class);
        $this->expectExceptionMessage(
            'Invalid event class [Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Unknown\Events\UnregisteredEvent] for event type [user.created], event class [Grifix\EventStore\Tests\Integration\Dummies\EventProducers\User\Events\UserCreatedEvent] expected!'
        );
        $this->eventStore->processEvent(
            new EventEnvelope(
                Uuid::createFromString('6e25260f-849d-4492-acd1-1d52f1172338'),
                'user',
                Uuid::createFromString('76a52fe9-8c12-42fa-9d13-0f2136f4ac23'),
                BigInt::create(1),
                new \DateTimeImmutable('2001-01-01'),
                'created',
                new UnregisteredEvent()
            )
        );
    }

    public function testItDoesNotProcessUnregisteredEvent(): void
    {
        $this->expectException(EventTypeDoesNotExistException::class);
        $this->expectExceptionMessage('Event type  [unknown.unregistered] does not exist!');
        $this->eventStore->processEvent(
            new EventEnvelope(
                Uuid::createFromString('6e25260f-849d-4492-acd1-1d52f1172338'),
                'unknown',
                Uuid::createFromString('76a52fe9-8c12-42fa-9d13-0f2136f4ac23'),
                BigInt::create(1),
                new \DateTimeImmutable('2001-01-01'),
                'unregistered',
                new UnregisteredEvent()
            )
        );
    }

    public function testItDoesNotAddStreamWithSameType(): void
    {
        $this->expectException(StreamTypeAlreadyExistsException::class);
        $this->expectExceptionMessage('Stream type with name [user] already exists!');
        $this->eventStore->registerStreamType('user', User::class);
    }

    public function testItDoesNotAddStreamWithSameProducerClass(): void
    {
        $this->expectException(StreamTypeAlreadyExistsException::class);
        $this->expectExceptionMessage(
            'Stream type with producer class [Grifix\EventStore\Tests\Integration\Dummies\EventProducers\User\User] already exists!'
        );
        $this->eventStore->registerStreamType('user2', User::class);
    }

    public function testItDoesNotStoreEventFromUnknownProducerClass(): void
    {
        $this->expectException(StreamTypeDoesNotExist::class);
        $this->expectExceptionMessage(
            'Stream type with producer class [Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Unknown\Unknown] does not exist!'
        );
        $this->eventStore->storeEvent(
            new UnregisteredEvent(),
            Unknown::class,
            Uuid::createFromString('da18dcca-848b-4212-9ef8-54c87c96a496')
        );
    }

    public function testItDoesNotStoreUnknownEvent(): void
    {
        $this->expectException(EventTypeDoesNotExistException::class);
        $this->expectExceptionMessage(
            'Event type for event class [Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Unknown\Events\UnregisteredEvent] does not exist!'
        );
        $this->eventStore->storeEvent(
            new UnregisteredEvent(),
            User::class,
            Uuid::createFromString('da18dcca-848b-4212-9ef8-54c87c96a496')
        );
    }

    public function testItDoesNotRegisterSubscriptionTypeForNotExistedStream(): void
    {
        $this->expectException(StreamTypeDoesNotExist::class);
        $this->expectExceptionMessage('Stream type with name [unknown] does not exist!');
        $this->eventStore->registerSubscriptionType(
            'test',
            Subscriber::class,
            'unknown',
            [
                UserCreatedEvent::class
            ]
        );
    }

    public function testItDoesNotStopFinishedSubscription(): void
    {
        $this->setUpSagaSubscriber();
        $this->eventStore->storeEvent(
            new UserCreatedEvent('a1dab744-1b01-4fb9-b9a7-802fbd7dd8ea', 'user@email.com'),
            User::class,
            Uuid::createFromString('a1dab744-1b01-4fb9-b9a7-802fbd7dd8ea')
        );
        $this->eventStore->storeEvent(
            new UserDeletedEvent('a1dab744-1b01-4fb9-b9a7-802fbd7dd8ea'),
            User::class,
            Uuid::createFromString('a1dab744-1b01-4fb9-b9a7-802fbd7dd8ea')
        );
        $this->eventStore->flush();
        $this->eventStore->publishEvents();
        $this->messageBroker->run();

        $this->expectException(CannotPauseSubscriptionException::class);
        $this->expectExceptionMessage(
            "Cannot pause subscription [test_subscription] for stream [a1dab744-1b01-4fb9-b9a7-802fbd7dd8ea] because it's status is [finished]!"
        );
        $this->eventStore->pauseSubscription(self::SUBSCRIPTION_TYPE, 'a1dab744-1b01-4fb9-b9a7-802fbd7dd8ea');
    }

    public function testItDoesNotProcessEventTwice(): void
    {
        $this->setUpProjectorSubscriber();
        $this->eventStore->processEvent(
            new EventEnvelope(
                Uuid::createFromString('1cfe4e7b-6178-4a40-8eda-4b596e9a1141'),
                'user',
                Uuid::createFromString('24f4e020-a374-4543-a850-b84b229dc4fd'),
                BigInt::create(1),
                new \DateTimeImmutable('2001-01-01'),
                'created',
                new UserCreatedEvent(
                    '24f4e020-a374-4543-a850-b84b229dc4fd',
                    'user@email.com'
                )
            )
        );
        $this->eventStore->processEvent(
            new EventEnvelope(
                Uuid::createFromString('1cfe4e7b-6178-4a40-8eda-4b596e9a1141'),
                'user',
                Uuid::createFromString('24f4e020-a374-4543-a850-b84b229dc4fd'),
                BigInt::create(1),
                new \DateTimeImmutable('2001-01-01'),
                'created',
                new UserCreatedEvent(
                    '24f4e020-a374-4543-a850-b84b229dc4fd',
                    'user2@email.com'
                )
            )
        );

        self::assertEquals('user@email.com', $this->projector->getAllRecords()[0]->payload);
    }

    public function testIdDoesNotRegisterSameSubscriptionType(): void
    {
        $this->expectException(SubscriptionTypeAlreadyExistsException::class);
        $this->expectExceptionMessage('Subscription type with name [test_subscription] already exists!');
        $this->eventStore->registerSubscriptionType(
            self::SUBSCRIPTION_TYPE,
            Subscriber::class,
            'user',
            [
                'user.created',
            ],
            [
                'user.deleted',
            ]
        );
    }

    private function publishEvent(object $event, string $producerClass, string $producerId): void
    {
        $this->eventStore->storeEvent(
            $event,
            $producerClass,
            Uuid::createFromString($producerId)
        );
        $this->eventStore->flush();
        $this->eventStore->publishEvents();
    }
}
