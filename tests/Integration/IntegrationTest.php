<?php

declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Logging\Middleware;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Grifix\Clock\FrozenClock;
use Grifix\EventStore\EventStore;
use Grifix\EventStore\Migrations\Version20220423084315;
use Grifix\EventStore\Tests\Integration\Mocks\LoggerHandler;
use Grifix\EventStore\Tests\Integration\Mocks\MessageBrokerMock;
use Grifix\Normalizer\Normalizer;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

abstract class IntegrationTest extends TestCase
{
    private array $migrations = [
        Version20220423084315::class
    ];

    protected readonly Connection $connection;

    protected readonly Logger $logger;

    protected readonly EventStore $eventStore;

    protected readonly FrozenClock $clock;

    protected readonly MessageBrokerMock $messageBroker;

    protected readonly LoggerHandler $loggerHandler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->loggerHandler = new LoggerHandler();
        $this->logger = new Logger('logger', [$this->loggerHandler]);
        $this->connection = $this->createConnection();
        $this->clock = new FrozenClock();
        $this->clock->freezeTime(new \DateTimeImmutable('2020-01-01 00:00:00.000001'));
        $this->messageBroker = new MessageBrokerMock();
        $this->eventStore = EventStore::create(
            $this->connection,
            $this->clock,
            $this->messageBroker,
            $this->logger,
            Normalizer::create()
        );
        $this->runMigrations();
    }

    private function createConnection(): Connection
    {
        $config = new Configuration();
        $config->setMiddlewares([
            new Middleware($this->logger)
        ]);
        return DriverManager::getConnection(
            [
                'dbname' => $_ENV['PHPUNIT_DB_NAME'],
                'user' => $_ENV['PHPUNIT_DB_USER'],
                'password' => $_ENV['PHPUNIT_DB_PASSWORD'],
                'host' => $_ENV['PHPUNIT_DB_HOST'],
                'driver' => 'pdo_pgsql'
            ],
            $config
        );
    }

    private function runMigrations(): void
    {
        if ($this->schemaExists('grifix_event_store')) {
            foreach ($this->migrations as $migrationClass) {
                /** @var AbstractMigration $migration */
                $migration = new $migrationClass($this->connection, $this->logger);
                $migration->down(new Schema());
                $this->executeMigration($migration);
            }
        }

        foreach ($this->migrations as $migrationClass) {
            /** @var AbstractMigration $migration */
            $migration = new $migrationClass($this->connection, $this->logger);
            $migration->up(new Schema());
            $this->executeMigration($migration);
        }
    }

    private function executeMigration(AbstractMigration $migration): void
    {
        foreach ($migration->getSql() as $sql) {
            $this->connection->executeQuery($sql->getStatement(), $sql->getParameters(), $sql->getTypes());
        }
    }

    private function schemaExists(string $schema): bool
    {
        return $this->connection->fetchOne(
            'SELECT EXISTS(SELECT 1 FROM information_schema.schemata WHERE schema_name = :schema)',
            ['schema' => $schema]
        );
    }
}
