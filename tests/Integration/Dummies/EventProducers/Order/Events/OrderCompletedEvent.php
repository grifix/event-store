<?php

declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Order\Events;

final class OrderCompletedEvent
{

    public function __construct(
        public readonly string $orderId,
        public readonly string $comment
    )
    {
    }
}
