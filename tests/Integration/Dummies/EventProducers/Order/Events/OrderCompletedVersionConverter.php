<?php

declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Order\Events;

use Grifix\Normalizer\VersionConverter\Exceptions\UnsupportedVersionException;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

final class OrderCompletedVersionConverter implements VersionConverterInterface
{

    public function convert(array $data, int $dataVersion, string $normalizerName): array
    {
        switch ($dataVersion) {
            case 1:
                $data['comment'] = '';
                break;
            default:
                throw new UnsupportedVersionException(
                    $normalizerName,
                    $dataVersion
                );
        }
        return $data;
    }
}
