<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Client;

use Grifix\EventStore\Tests\Integration\Dummies\Repository\Entity;

final class Client extends Entity
{

    public function __construct(string $id, string $name)
    {
        parent::__construct($id, 'client', $name);
    }

    public function getName(): string
    {
        return $this->value;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->value = $name;
    }
}
