<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Client\Events;

final class ClientDeletedEvent
{
    public function __construct(public readonly string $clientId)
    {
    }
}
