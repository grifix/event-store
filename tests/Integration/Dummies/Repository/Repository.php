<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Dummies\Repository;

use Doctrine\DBAL\Connection;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\Client\Client;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\User\User;

final class Repository
{

    private const TABLE = 'entities';

    public function __construct(private readonly Connection $connection)
    {
        $this->connection->executeQuery('drop table if exists ' . self::TABLE);
        $this->connection->executeQuery('
            create table if not exists ' . self::TABLE . ' 
            (
                id uuid    not null,
                type text not null,
                value  text
            );
        ');
    }

    public function add(Entity $entity): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'id' => $entity->id,
                'type' => $entity->type,
                'value' => $entity->value
            ]
        );
    }

    public function update(Entity $entity): void
    {
        $this->connection->update(
            self::TABLE,
            [
                'type' => $entity->type,
                'value' => $entity->value
            ],
            [
                'id' => $entity->id
            ]
        );
    }

    public function delete(string $id): void
    {
        $this->connection->delete(self::TABLE, ['id' => $id]);
    }

    public function findUser(string $id): ?User
    {
        $row = $this->getRow($id, 'user');
        if (!$row) {
            return null;
        }
        return new User(
            $row['id'],
            $row['value']
        );
    }

    public function findClient(string $id): ?Client
    {
        $row = $this->getRow($id, 'client');
        if (!$row) {
            return null;
        }
        return new Client(
            $row['id'],
            $row['value']
        );
    }

    private function getRow(string $id, string $type): ?array
    {
        $result = $this->connection->fetchAssociative(
            'select * from ' . self::TABLE . ' where id=:id and type=:type for update',
            [
                'id' => $id,
                'type' => $type
            ]
        );
        if (!$result) {
            return null;
        }
        return $result;
    }
}
