<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Mocks;

use Monolog\Handler\HandlerInterface;

final class LoggerHandler implements HandlerInterface
{
    private array $records = [];

    public function handleBatch(array $records): void
    {
        foreach ($records as $record) {
            $this->handle($record);
        }
    }

    public function handle(array $record): bool
    {
        $this->records[] = $record;
        return true;
    }

    public function close(): void
    {

    }

    public function isHandling(array $record): bool
    {
        return true;
    }

    public function getRecords(): array
    {
        return $this->records;
    }

    public function clear(): void
    {
        $this->records = [];
    }

    public function shiftRecord(): array
    {
        return array_shift($this->records);
    }
}
