<?php

declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\EventRepository;

use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\User\Events\UserDeletedEvent;
use Grifix\EventStore\Tests\Integration\Dummies\EventProducers\User\User;
use Grifix\EventStore\Tests\Integration\IntegrationTest;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Uuid\Uuid;

final class EventRepositoryTest extends IntegrationTest
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->eventStore->registerStreamType('user', User::class);
        $this->eventStore->registerEventType(
            'user',
            'deleted',
            UserDeletedEvent::class,
            [
                Schema::create()
                ->withStringProperty('userId')
            ],
        );
    }

    public function testItSplitsToChunksProperly()
    {
        for ($i = 0; $i < 100; $i++) {
            $this->eventStore->storeEvent(
                new UserDeletedEvent('8842841a-8e88-4f5a-8c3e-968067eca09a'),
                User::class,
                Uuid::createFromString('8842841a-8e88-4f5a-8c3e-968067eca09a')
            );
        }
        $this->eventStore->flush();
        $this->loggerHandler->clear();
        $envelopes = array(...$this->eventStore->findEvents(chunkSize: 10));
        self::assertCount(100, $envelopes);
        self::assertCount(11, $this->loggerHandler->getRecords());
    }
}
