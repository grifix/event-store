<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Unit;

use Grifix\EventStore\Cli\PauseSubscriptionCommand;
use Grifix\EventStore\Cli\RunEventConsumerCommand;
use Grifix\EventStore\Cli\RunEventPublisherWorkerCommand;
use Grifix\EventStore\Cli\UnPauseSubscriptionCommand;
use Grifix\EventStore\EventStoreInterface;
use Grifix\EventStore\MessageBroker\MessageBrokerInterface;
use Grifix\Worker\WorkerFactoryInterface;
use Grifix\Worker\WorkerInterface;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

final class CliTest extends TestCase
{
    public function testItPausesSubscription(): void
    {
        /** @var EventStoreInterface|Mock $eventStore */
        $eventStore = \Mockery::mock(EventStoreInterface::class)
            ->shouldReceive('pauseSubscription')->withArgs([
                'test',
                '4aa5805c-6c7c-4302-8a58-c8df5fa27057'
            ])->getMock();
        $command = new PauseSubscriptionCommand($eventStore);
        $this->executeCommand(
            $command,
            [
                PauseSubscriptionCommand::ARG_STREAM_ID => 'test',
                PauseSubscriptionCommand::ARG_SUBSCRIPTION_TYPE => '4aa5805c-6c7c-4302-8a58-c8df5fa27057'
            ]
        );
        self::assertTrue(true);
    }

    public function testItUnpausesSubscription(): void
    {
        /** @var EventStoreInterface|Mock $eventStore */
        $eventStore = \Mockery::mock(EventStoreInterface::class)
            ->shouldReceive('unpauseSubscription')->withArgs([
                'test',
                '4aa5805c-6c7c-4302-8a58-c8df5fa27057'
            ])->getMock();
        $command = new UnPauseSubscriptionCommand($eventStore);
        $this->executeCommand(
            $command,
            [
                UnPauseSubscriptionCommand::ARG_STREAM_ID => 'test',
                UnPauseSubscriptionCommand::ARG_SUBSCRIPTION_TYPE => '4aa5805c-6c7c-4302-8a58-c8df5fa27057'
            ]
        );
        self::assertTrue(true);
    }

    public function testItRunsEventConsumer(): void
    {

        /** @var MessageBrokerInterface $messageBroker */
        $messageBroker = \Mockery::mock(MessageBrokerInterface::class)
            ->shouldReceive('startConsumer')->getMock();

        /** @var EventStoreInterface $eventStore */
        $eventStore = \Mockery::mock(EventStoreInterface::class);
        $command = new RunEventConsumerCommand($messageBroker, $eventStore);
        $this->executeCommand($command);
        self::assertTrue(true);
    }

    public function testItRunsEventPublisherWorker(): void
    {
        $worker = \Mockery::mock(WorkerInterface::class)->shouldReceive('run')->getMock();
        /** @var WorkerFactoryInterface $workerFactory */
        $workerFactory = \Mockery::mock(WorkerFactoryInterface::class)
            ->shouldReceive('create')
            ->andReturn($worker)
            ->getMock();

        /** @var EventStoreInterface $eventStore */
        $eventStore = \Mockery::mock(EventStoreInterface::class);
        $command = new RunEventPublisherWorkerCommand($eventStore, $workerFactory);
        $this->executeCommand($command);
        self::assertTrue(true);
    }

    private function executeCommand(Command $command, array $input = []): void
    {
        $tester = new CommandTester($command);
        $tester->execute($input);
    }

}
