<?php

declare(strict_types=1);

namespace Grifix\EventStore;

use Doctrine\DBAL\Connection;
use Grifix\BigInt\BigInt;
use Grifix\Clock\ClockInterface;
use Grifix\EventStore\DependencyProvider\DependencyProviderInterface;
use Grifix\EventStore\DependencyProvider\MemoryDependencyProvider;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\Event\Repository\DbalEventRepository;
use Grifix\EventStore\Event\Repository\EventRepositoryInterface;
use Grifix\EventStore\EventType\EventType;
use Grifix\EventStore\EventType\Repository\EventTypeRepositoryInterface;
use Grifix\EventStore\EventType\Repository\Exceptions\EventTypeAlreadyExistsException;
use Grifix\EventStore\EventType\Repository\MemoryEventTypeRepository;
use Grifix\EventStore\MessageBroker\MessageBrokerInterface;
use Grifix\EventStore\StreamType\Repository\MemoryStreamTypeRepository;
use Grifix\EventStore\StreamType\Repository\StreamTypeRepositoryInterface;
use Grifix\EventStore\StreamType\StreamType;
use Grifix\EventStore\Subscription\Repository\SubscriptionRepository;
use Grifix\EventStore\Subscription\Repository\SubscriptionRepositoryInterface;
use Grifix\EventStore\Subscription\SubscriptionDto;
use Grifix\EventStore\Subscription\SubscriptionFactory;
use Grifix\EventStore\Subscription\SubscriptionNormalizer;
use Grifix\EventStore\Subscription\SubscriptionOutside;
use Grifix\EventStore\SubscriptionType\Repository\MemorySubscriptionTypeRepository;
use Grifix\EventStore\SubscriptionType\Repository\SubscriptionTypeRepositoryInterface;
use Grifix\EventStore\SubscriptionType\SubscriptionType;
use Grifix\EventStore\Workers\EventProcessor;
use Grifix\EventStore\Workers\EventPublisher;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Uuid\Uuid;
use Psr\Log\LoggerInterface;

class EventStore implements EventStoreInterface
{
    public function __construct(
        private readonly StreamTypeRepositoryInterface $streamTypeRepository,
        private readonly EventTypeRepositoryInterface $eventTypeRepository,
        private readonly SubscriptionTypeRepositoryInterface $subscriptionTypeRepository,
        private readonly EventRepositoryInterface $eventRepository,
        private readonly SubscriptionRepositoryInterface $subscriptionRepository,
        private readonly EventPublisher $eventPublisher,
        private readonly EventProcessor $eventProcessor,
        private readonly DependencyProviderInterface $dependencyProvider
    ) {
    }

    public function registerStreamType(string $name, ?string $producerClass): void
    {
        $this->streamTypeRepository->add(
            new StreamType($this->eventTypeRepository, $name, $producerClass)
        );
    }

    /**
     * @param Schema[] $schemas
     * @throws EventTypeAlreadyExistsException
     */
    public function registerEventType(
        string $streamTypeName,
        string $name,
        string $eventClass,
        array $schemas,
        ?string $versionConverterClass = null
    ): void {
        $this->eventTypeRepository->add(
            new EventType(
                $streamTypeName,
                $name,
                $eventClass,
                $schemas,
                $versionConverterClass
            )
        );
    }

    public function registerSubscriptionType(
        string $name,
        string $subscriberClass,
        string $streamType,
        array $staringEvents,
        array $finishingEvents = []
    ): void {
        $this->subscriptionTypeRepository->add(
            new SubscriptionType(
                $name,
                $subscriberClass,
                $this->streamTypeRepository->getByName($streamType),
                $staringEvents,
                $finishingEvents
            )
        );
    }

    public function unpauseSubscription(?string $subscriptionType, ?string $subscriptionId): void
    {
        $this->subscriptionRepository->get($subscriptionType, $subscriptionId)->unpause();
    }

    public function pauseSubscription(string $subscriptionType, string $subscriptionId): void
    {
        $this->subscriptionRepository->get($subscriptionType, $subscriptionId)->pause();
    }

    public function resetSubscription(?string $subscriptionType, ?string $subscriptionId): void
    {
        // TODO: Implement resetSubscription() method.
    }

    public function storeEvent(object $event, string $producer, Uuid $streamId): void
    {
        if(class_exists($producer)){
            $streamType = $this->streamTypeRepository->getByProducerClass($producer);
        }else{
            $streamType = $this->streamTypeRepository->getByName($producer);
        }

        $this->eventRepository->add($event, $streamType, $streamId);
    }

    /**
     * @inheritdoc
     */
    public function findSubscriptions(?string $type = null, ?string $streamId = null, ?string $status = null): iterable
    {
        return $this->subscriptionRepository->find($type, $streamId, $status);
    }

    public function flush(): void
    {
        $this->eventRepository->flush();
        $this->subscriptionRepository->flush();
    }

    public function publishEvents(): void
    {
        $this->eventPublisher->__invoke();
    }

    public function processEvent(EventEnvelope $envelope): void
    {
        $this->eventProcessor->__invoke($envelope);
    }

    public function registerSubscriber(object $subscriber): void
    {
        $this->dependencyProvider->set($subscriber);
    }

    public function registerVersionConverter(object $versionConverter): void
    {
        $this->dependencyProvider->set($versionConverter);
    }

    /**
     * @return EventEnvelope[]
     */
    public function findEvents(
        ?bool $published = null,
        ?BigInt $fromNumber = null,
        string $streamId = null,
        ?int $limit = null,
        int $chunkSize = 1000,
        ?string $streamType = null,
    ): iterable {
        return $this->eventRepository->find($published, $fromNumber, $streamId, $limit, $chunkSize, $streamType);
    }

    public static function create(
        Connection $connection,
        ClockInterface $clock,
        MessageBrokerInterface $messageBroker,
        LoggerInterface $logger,
        NormalizerInterface $normalizer,
    ): EventStoreInterface {
        $streamTypeRepository = new MemoryStreamTypeRepository();
        $dependencyProvider = new MemoryDependencyProvider();
        $eventTypeRepository = new MemoryEventTypeRepository($normalizer, $dependencyProvider);
        $subscriptionTypeRepository = new MemorySubscriptionTypeRepository();
        $eventRepository = new DbalEventRepository($connection, $clock, $eventTypeRepository, $normalizer);
        $subscriptionOutside = new SubscriptionOutside($dependencyProvider);
        $subscriptionNormalizer = new SubscriptionNormalizer($subscriptionOutside, $subscriptionTypeRepository);
        $subscriptionRepository = new SubscriptionRepository($connection, $subscriptionNormalizer);
        $eventPublisher = new EventPublisher($eventRepository, $messageBroker);
        $subscriptionFactory = new SubscriptionFactory($subscriptionOutside, $subscriptionTypeRepository);
        $eventProcessor = new EventProcessor(
            $connection,
            $subscriptionRepository,
            $subscriptionTypeRepository,
            $subscriptionFactory,
            $eventRepository,
            $logger,
            $eventTypeRepository
        );


        return new self(
            $streamTypeRepository,
            $eventTypeRepository,
            $subscriptionTypeRepository,
            $eventRepository,
            $subscriptionRepository,
            $eventPublisher,
            $eventProcessor,
            $dependencyProvider
        );
    }
}
