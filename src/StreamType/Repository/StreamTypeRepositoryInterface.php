<?php
declare(strict_types=1);

namespace Grifix\EventStore\StreamType\Repository;

use Grifix\EventStore\StreamType\Repository\Exceptions\StreamTypeAlreadyExistsException;
use Grifix\EventStore\StreamType\Repository\Exceptions\StreamTypeDoesNotExist;
use Grifix\EventStore\StreamType\StreamType;

interface StreamTypeRepositoryInterface
{
    /**
     * @throws StreamTypeAlreadyExistsException
     */
    public function add(StreamType $newStreamType): void;

    /**
     * @throws StreamTypeDoesNotExist
     */
    public function getByProducerClass(string $producerClass): StreamType;

    public function getByName(string $name): StreamType;
}
