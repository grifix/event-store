<?php

declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Repository;

use Grifix\EventStore\Subscription\Repository\Exceptions\SubscriptionDoesNotExitsException;
use Grifix\EventStore\Subscription\Subscription;
use Grifix\EventStore\Subscription\SubscriptionDto;

interface SubscriptionRepositoryInterface
{
    public function flush(): void;

    /**o
     * @throws SubscriptionDoesNotExitsException
     */
    public function get(string $subscriptionType, string $streamId): Subscription;

    public function has(string $subscriptionType, string $streamId): bool;

    public function add(Subscription $subscription): void;

    public function delete(string $subscriptionType, string $streamId): void;

    /**
     * @return SubscriptionDto[]
     */
    public function find(
        ?string $type = null,
        ?string $streamId = null,
        ?string $status = null
    ): iterable;

    public function clear(): void;
}
