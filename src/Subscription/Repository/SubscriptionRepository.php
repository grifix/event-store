<?php

declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Repository;

use Doctrine\DBAL\Connection;
use Grifix\BigInt\BigInt;
use Grifix\EventStore\Subscription\Repository\Exceptions\SubscriptionDoesNotExitsException;
use Grifix\EventStore\Subscription\Subscription;
use Grifix\EventStore\Subscription\SubscriptionDto;
use Grifix\EventStore\Subscription\SubscriptionNormalizer;
use Grifix\Uuid\Uuid;

final class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    private const TABLE = 'grifix_event_store.subscriptions';

    private SubscriptionBuffer $buffer;

    public function __construct(
        private readonly Connection $connection,
        private readonly SubscriptionNormalizer $normalizer
    ) {
        $this->buffer = new SubscriptionBuffer();
    }


    public function flush(): void
    {
        while (!$this->buffer->isEmpty()) {
            foreach ($this->buffer as $subscription) {
                /** @var Subscription $subscription */
                $this->persistSubscription($subscription);
                $this->buffer->remove($subscription->getType(), $subscription->getStreamId());
            }
        }
    }

    /**
     * @throws SubscriptionDoesNotExitsException
     */
    public function get(string $subscriptionType, string $streamId): Subscription
    {
        $result = $this->buffer->get($subscriptionType, $streamId);
        if (null !== $result) {
            return $result;
        }

        $data = $this->getData($subscriptionType, $streamId);
        if (null === $data) {
            throw new SubscriptionDoesNotExitsException($subscriptionType, $streamId);
        }
        $result = $this->normalizer->denormalize($data);
        $this->buffer->add($result);
        return $result;
    }

    public function has(string $subscriptionType, string $streamId): bool
    {
        $result = $this->connection->fetchFirstColumn(
            'select count(*) from '.self::TABLE.' where stream_id = :stream_id and type = :type',
            [
                'stream_id' => $streamId,
                'type' => $subscriptionType
            ]
        );
        return $result[0] > 0;
    }

    public function find(
        ?string $type = null,
        ?string $streamId = null,
        ?string $status = null
    ): iterable {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder->select('*')->from(self::TABLE);

        if (null !== $type) {
            $queryBuilder->where('type = :type')
                ->setParameter('type', $type);
        }

        if (null !== $streamId) {
            $queryBuilder->where('stream_id = :stream_id')
                ->setParameter('stream_id', $streamId);
        }

        if (null !== $status) {
            $queryBuilder->where('status = :status')
                ->setParameter('status', $status);
        }

        foreach ($queryBuilder->fetchAllAssociative() as $row) {
            yield new SubscriptionDto(
                $row['type'],
                Uuid::createFromString($row['stream_id']),
                $row['status'],
                BigInt::create($row['last_received_event_number']),
            );
        }
    }

    private function getData(string $subscriptionType, string $streamId): ?array
    {
        $result = $this->connection->fetchAssociative(
            'select * from '.self::TABLE.' where stream_id = :stream_id and type = :type for update',
            [
                'stream_id' => $streamId,
                'type' => $subscriptionType
            ]
        );
        if (!$result) {
            return null;
        }
        return $result;
    }

    public function add(Subscription $subscription): void
    {
        $this->buffer->add($subscription);
    }

    public function delete(string $subscriptionType, string $streamId): void
    {
        $this->connection->delete(
            self::TABLE,
            [
                'type' => $subscriptionType,
                'stream_id' => $streamId
            ]
        );
    }

    private function persistSubscription(Subscription $subscription): void
    {
        $data = $this->normalizer->normalize($subscription);
        if (null === $this->getData($subscription->getType(), $subscription->getStreamId())) {
            $this->connection->insert(
                self::TABLE,
                $data
            );
            return;
        }
        unset($data['type'], $data['streamId']);

        $this->connection->update(
            self::TABLE,
            $data,
            [
                'stream_id' => $subscription->getStreamId(),
                'type' => $subscription->getType()
            ]
        );
    }

    public function clear(): void
    {
        $this->buffer->clear();
    }
}
