<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Repository\Exceptions;

final class SubscriptionDoesNotExitsException extends \Exception
{

    public function __construct(string $subscriptionType, string $streamId)
    {
        parent::__construct(sprintf('Subscription [%s] for stream [%s] does not exist', $subscriptionType, $streamId));
    }
}
