<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Status\Exceptions;

use Exception;

final class CannotChangeSubscriptionStatus extends Exception
{

    public function __construct(public readonly string $fromStatus, public readonly string $toStatus)
    {
        parent::__construct(
            sprintf('Cannot change subscription status from [%s] to [%s]!', $fromStatus, $toStatus)
        );
    }
}
