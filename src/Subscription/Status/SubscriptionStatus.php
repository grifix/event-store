<?php

declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Status;

use Grifix\EventStore\Subscription\Status\Exceptions\CannotChangeSubscriptionStatus;
use Grifix\StateMachine\Exceptions\TransitionIsNotPossibleException;
use Grifix\StateMachine\StateMachine;
use Grifix\StateMachine\Transition;
use Grifix\Uuid\Uuid;

final class SubscriptionStatus
{
    private const ACTIVE = 'active';

    private const PAUSED = 'paused';

    private const FINISHED = 'finished';

    private static ?StateMachine $stateMachine = null;


    private function __construct(private readonly string $value, ?string $fromValue = null)
    {
        self::getStateMachine()->assertTransitionIsPossible($fromValue, $value);
    }

    public static function create(Uuid $subscriptionId): self
    {
        return new self(
            self::ACTIVE
        );
    }

    /**
     * @throws CannotChangeSubscriptionStatus
     */
    public function pause(): self
    {
        return $this->changeValue(self::PAUSED);
    }

    /**
     * @throws CannotChangeSubscriptionStatus
     */
    public function unpause(): self
    {
        return $this->changeValue(self::ACTIVE);
    }

    /**
     * @throws CannotChangeSubscriptionStatus
     */
    public function finish(): self
    {
        return $this->changeValue(self::FINISHED);
    }

    public function isActive(): bool
    {
        return $this->value === self::ACTIVE;
    }

    /**
     * @throws CannotChangeSubscriptionStatus
     */
    private function changeValue(string $newState): self
    {
        try {
            return new self($newState, $this->value);
        } catch (TransitionIsNotPossibleException $exception) {
            throw new CannotChangeSubscriptionStatus($this->value, $newState);
        }
    }

    private static function getStateMachine(): StateMachine
    {
        if (null === self::$stateMachine) {
            self::$stateMachine = new StateMachine(
                new Transition(null, self::ACTIVE),
                new Transition(self::ACTIVE, self::PAUSED, self::FINISHED),
                new Transition(self::PAUSED, self::ACTIVE)
            );
        }

        return self::$stateMachine;
    }
}
