<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription;

use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\SubscriptionType\Repository\SubscriptionTypeRepositoryInterface;

final class SubscriptionFactory
{

    public function __construct(
        private readonly SubscriptionOutsideInterface $outside,
        private readonly SubscriptionTypeRepositoryInterface $subscriptionTypeRepository
    )
    {
    }

    public function create(string $type, EventEnvelope $statingEvent): Subscription
    {
        return new Subscription(
            $this->outside,
            $this->subscriptionTypeRepository->getByName($type),
            $statingEvent
        );
    }
}
