<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Exceptions;

use Exception;

final class WrongEventStreamException extends Exception
{

    public function __construct(string $subscriptionType, string $subscriptionStreamId, string $eventStreamId, string $eventType)
    {
        parent::__construct(
            sprintf(
                'Event [%s] is from stream [%s] but subscription [%s] stream is [%s]!',
                $eventType,
                $eventStreamId,
                $subscriptionType,
                $subscriptionStreamId
            )
        );
    }
}
