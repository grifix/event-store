<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Exceptions;

final class SubscriptionIsNotActiveException extends \Exception
{

    public function __construct(string $subscriptionId)
    {
        parent::__construct(sprintf('Subscription [%s] is not active!', $subscriptionId));
    }
}
