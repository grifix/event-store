<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Exceptions;

use Exception;

final class InvalidEventNumberException extends Exception
{

    public function __construct(
        public readonly string $subscriptionId,
        public readonly string $actualNumber,
        public readonly string $expectedNumber
    )
    {
        parent::__construct(
            sprintf(
                'Invalid event number for subscription [%s], expected number is[%s] but [%s] received',
                $subscriptionId,
                $expectedNumber,
                $actualNumber
            )
        );
    }
}
