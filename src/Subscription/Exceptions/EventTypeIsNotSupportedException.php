<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Exceptions;

use Exception;

final class EventTypeIsNotSupportedException extends Exception
{

    public function __construct(
        public readonly string $eventType,
        public readonly string $subscriptionType,
        public readonly string $subscriptionId
    )
    {
        parent::__construct(
            sprintf(
                'Event type [%s] is not supported by subscription type [%s] with id [%s]',
                $eventType,
                $subscriptionType,
                $subscriptionId
            )
        );
    }
}
