<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Exceptions;

final class EventIsNotStartingException extends \Exception
{

    public function __construct(public readonly string $eventType, public readonly string $subscriptionType)
    {
        parent::__construct(sprintf('Event type [%s] is not starting for subscription type [%s]!', $eventType, $subscriptionType));
    }
}
