<?php

declare(strict_types=1);

namespace Grifix\EventStore\Subscription;

use Grifix\EventStore\DependencyProvider\DependencyProviderInterface;

final class SubscriptionOutside implements SubscriptionOutsideInterface
{
    public function __construct(private readonly DependencyProviderInterface $dependencyProvider)
    {
    }

    public function getSubscriber(string $subscriberClass): object
    {
        return $this->dependencyProvider->get($subscriberClass);
    }
}
