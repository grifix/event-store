<?php

declare(strict_types=1);

namespace Grifix\EventStore\Subscription;

use Grifix\BigInt\BigInt;
use Grifix\Uuid\Uuid;

final class SubscriptionDto
{

    public function __construct(
        public readonly string $type,
        public readonly Uuid $streamId,
        public readonly string $status,
        public readonly BigInt $lastReceivedEventNumber
    ) {
    }
}
