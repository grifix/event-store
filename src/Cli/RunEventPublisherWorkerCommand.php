<?php
declare(strict_types=1);

namespace Grifix\EventStore\Cli;

use Grifix\EventStore\EventStoreInterface;
use Grifix\Worker\AbstractWorkerCommand;
use Grifix\Worker\WorkerFactory;
use Grifix\Worker\WorkerFactoryInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class RunEventPublisherWorkerCommand extends AbstractWorkerCommand
{
    public const NAME = 'grifix:event-store:run-event-publisher-worker';
    protected static $defaultName = self::NAME;
    protected static $defaultDescription = 'Sends all unpublished events to the message broker';

    public function __construct(
        private       readonly EventStoreInterface $eventStore,
        WorkerFactoryInterface $workerFactory
    )
    {
        parent::__construct($workerFactory);
    }

    protected function getCallback(InputInterface $input, OutputInterface $output): callable
    {
        return function () {
            $this->eventStore->publishEvents();
        };
    }
}
