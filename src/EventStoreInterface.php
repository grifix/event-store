<?php

declare(strict_types=1);

namespace Grifix\EventStore;

use Grifix\BigInt\BigInt;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\EventType\Repository\Exceptions\EventTypeAlreadyExistsException;
use Grifix\EventStore\Subscription\SubscriptionDto;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Uuid\Uuid;

interface EventStoreInterface
{
    public function registerStreamType(string $name, ?string $producerClass): void;

    /**
     * @param Schema[] $schemas
     * @throws EventTypeAlreadyExistsException
     */
    public function registerEventType(
        string $streamTypeName,
        string $name,
        string $eventClass,
        array $schemas,
        ?string $versionConverterClass = null
    ): void;

    public function registerSubscriptionType(
        string $name,
        string $subscriberClass,
        string $streamType,
        array $staringEvents,
        array $finishingEvents = []
    ): void;

    public function registerSubscriber(object $subscriber): void;

    public function unpauseSubscription(string $subscriptionType, string $subscriptionId): void;

    public function pauseSubscription(string $subscriptionType, string $subscriptionId): void;

    public function resetSubscription(string $subscriptionType, string $subscriptionId): void;

    public function storeEvent(object $event, string $producer, Uuid $streamId): void;

    public function flush(): void;

    public function publishEvents(): void;

    public function processEvent(EventEnvelope $envelope): void;

    /**
     * @return EventEnvelope[]
     */
    public function findEvents(
        ?bool $published = null,
        ?BigInt $fromNumber = null,
        string $streamId = null,
        ?int $limit = null,
        int $chunkSize = 1000,
        ?string $streamType = null
    ): iterable;

    /**
     * @return SubscriptionDto[]
     */
    public function findSubscriptions(?string $type = null, ?string $streamId = null, ?string $status = null): iterable;

    public function registerVersionConverter(object $versionConverter): void;
}

