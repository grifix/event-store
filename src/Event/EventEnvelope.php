<?php
declare(strict_types=1);

namespace Grifix\EventStore\Event;

use DateTimeImmutable;
use Grifix\BigInt\BigInt;
use Grifix\Uuid\Uuid;

final class EventEnvelope
{


    public function __construct(
        public readonly Uuid $id,
        public readonly string $streamTypeName,
        public readonly Uuid $streamId,
        public readonly BigInt $number,
        public readonly DateTimeImmutable $dateOfCreation,
        public readonly string $eventTypeName,
        public readonly object $event,
        public readonly ?DateTimeImmutable $dateOfPublishing = null
    )
    {
    }

    public function getEventType(): string
    {
        return $this->streamTypeName . '.' . $this->eventTypeName;
    }

}
