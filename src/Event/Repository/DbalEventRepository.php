<?php

declare(strict_types=1);

namespace Grifix\EventStore\Event\Repository;

use Doctrine\DBAL\Connection;
use Grifix\BigInt\BigInt;
use Grifix\Clock\ClockInterface;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\EventType\Repository\EventTypeRepositoryInterface;
use Grifix\EventStore\StreamType\StreamType;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\Uuid\Uuid;

final class DbalEventRepository implements EventRepositoryInterface
{
    /** @var EventEnvelope[] */
    private array $events;

    private const TABLE = 'grifix_event_store.events';

    private const DATE_FORMAT = 'Y-m-d H:i:s.u';

    public function __construct(
        private readonly Connection $connection,
        private readonly ClockInterface $clock,
        private readonly EventTypeRepositoryInterface $eventTypeRepository,
        private readonly NormalizerInterface $normalizer,
    ) {
        $this->events = [];
    }

    public function find(
        ?bool $published = null,
        ?BigInt $fromNumber = null,
        ?string $streamId = null,
        ?int $limit = null,
        ?int $chunkSize = 1000,
        ?string $streamType = null,
    ): iterable {
        $query = $this->connection->createQueryBuilder()->select('*')
            ->from(self::TABLE);
        if (true === $published) {
            $query->andWhere('date_of_publishing is not null');
        }
        if (false === $published) {
            $query->andWhere('date_of_publishing is null');
        }
        if ($fromNumber) {
            $query->andWhere('number >= :from_number')
                ->setParameter('from_number', $fromNumber->toString());
        }

        if ($streamId) {
            $query->andWhere('stream_id = :stream_id')
                ->setParameter('stream_id', $streamId);
        }

        if ($streamType) {
            $query->andWhere('stream_type_name = :stream_type')
                ->setParameter('stream_type', $streamType);
        }
        $query->orderBy('date_of_creation', 'asc');

        $query->setFirstResult(0);

        if ($limit) {
            $query->setMaxResults($limit);
            $rows = $query->fetchAllAssociative();
            yield from $this->createEvents($rows);
        } else {
            $query->setMaxResults($chunkSize);
            $rows = $query->fetchAllAssociative();
            while ($rows) {
                yield from $this->createEvents($rows);
                $query->setFirstResult($query->getFirstResult() + $chunkSize);
                $rows = $query->fetchAllAssociative();;
            }
        }
    }

    private function createEvents(array $rows): iterable
    {
        foreach ($rows as $row) {
            yield $this->createEvent($row);
        }
    }

    private function createEvent($row): EventEnvelope
    {
        $dateOfPublishing = null;
        if ($row['date_of_publishing']) {
            $dateOfPublishing = new \DateTimeImmutable($row['date_of_publishing']);
        }
        return new EventEnvelope(
            Uuid::createFromString($row['id']),
            $row['stream_type_name'],
            Uuid::createFromString($row['stream_id']),
            BigInt::create($row['number']),
            new \DateTimeImmutable($row['date_of_creation']),
            $row['event_type_name'],
            $this->normalizer->denormalize(json_decode($row['event'], true)),
            $dateOfPublishing
        );
    }

    public function add(object $event, StreamType $streamType, Uuid $streamId): void
    {
        $this->events[] = new EventEnvelope(
            Uuid::createRandom(),
            $streamType->toString(),
            $streamId,
            $this->getNextEventNumber($streamType->toString(), $streamId->toString()),
            $this->clock->getCurrentTime(),
            $this->eventTypeRepository->getByEventClass($event::class)->name,
            $event
        );
    }

    public function flush(): void
    {
        while (!empty($this->events)) {
            foreach ($this->events as $i => $envelope) {
                $this->insetEvent($envelope);
                //TODO run sync subscriptions
                unset($this->events[$i]);
            }
        }
    }

    private function insetEvent(EventEnvelope $envelope): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'id' => $envelope->id->toString(),
                'stream_type_name' => $envelope->streamTypeName,
                'stream_id' => $envelope->streamId->toString(),
                'number' => $envelope->number->toString(),
                'date_of_creation' => $envelope->dateOfCreation->format(self::DATE_FORMAT),
                'event_type_name' => $envelope->eventTypeName,
                'event' => json_encode($this->normalizer->normalize($envelope->event))
            ]
        );
    }

    private function getNextEventNumber(string $streamType, string $streamId): ?BigInt
    {
        $lastEventNumber = $this->getLastEventNumber($streamType, $streamId);
        if (null === $lastEventNumber) {
            $lastEventNumber = $this->selectLastEventNumber($streamType, $streamId);
        }
        return $lastEventNumber->add(BigInt::create(1));
    }

    private function getLastEventNumber($streamType, $streamId): ?BigInt
    {
        $events = array_reverse($this->events);
        foreach ($events as $event) {
            if ($event->streamTypeName === $streamType && $event->streamId->toString() === $streamId) {
                return $event->number;
            }
        }
        return null;
    }

    private function selectLastEventNumber(string $streamType, string $streamId): BigInt
    {
        $record = $this->connection->fetchAssociative(
            'select max(number) as number from '.self::TABLE.' where stream_id=? and stream_type_name=?',
            [$streamId, $streamType]
        );
        return BigInt::create($record['number'] ?? 0);
    }

    public function markAsSent(string $eventId): void
    {
        $this->connection->update(
            self::TABLE,
            [
                'date_of_publishing' => $this->clock->getCurrentTime()->format(self::DATE_FORMAT)
            ],
            [
                'id' => $eventId
            ]
        );
    }

}
