<?php
declare(strict_types=1);

namespace Grifix\EventStore\EventType\Repository;

use Grifix\EventStore\EventType\EventType;
use Grifix\EventStore\EventType\Repository\Exceptions\EventTypeAlreadyExistsException;
use Grifix\EventStore\EventType\Repository\Exceptions\EventTypeDoesNotExistException;

interface EventTypeRepositoryInterface
{
    /**
     * @throws EventTypeAlreadyExistsException
     */
    public function add(EventType $newEventType): void;

    /**
     * @throws EventTypeDoesNotExistException
     */
    public function getByEventClass(string $eventClass): EventType;

    /**
     * @throws EventTypeDoesNotExistException
     */
    public function getByEventType(string $eventType): EventType;
}
