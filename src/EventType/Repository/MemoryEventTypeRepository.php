<?php

declare(strict_types=1);

namespace Grifix\EventStore\EventType\Repository;

use Grifix\EventStore\DependencyProvider\DependencyProviderInterface;
use Grifix\EventStore\EventType\EventType;
use Grifix\EventStore\EventType\Repository\Exceptions\EventTypeAlreadyExistsException;
use Grifix\EventStore\EventType\Repository\Exceptions\EventTypeDoesNotExistException;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\Normalizer\VersionConverter\VersionConverterInterface;

final class MemoryEventTypeRepository implements EventTypeRepositoryInterface
{


    /**
     * @var EventType[]
     */
    private array $eventTypes = [];

    public function __construct(
        private readonly NormalizerInterface $normalizer,
        private readonly DependencyProviderInterface $dependencyProvider
    ) {
    }

    public function add(EventType $newEventType): void
    {
        foreach ($this->eventTypes as $eventType) {
            if ($eventType->toString() === $newEventType->toString()) {
                throw EventTypeAlreadyExistsException::withName($newEventType->toString());
            }
            if ($eventType->eventClass === $newEventType->eventClass) {
                throw EventTypeAlreadyExistsException::withEventClass($newEventType->eventClass);
            }
        }
        $this->eventTypes[] = $newEventType;
        $versionConverter = null;
        if($newEventType->versionConverterClass){
            /** @var VersionConverterInterface $versionConverter */
            $versionConverter = $this->dependencyProvider->get($newEventType->versionConverterClass);
        }
        $this->normalizer->registerDefaultObjectNormalizer(
            $newEventType->toString(),
            $newEventType->eventClass,
            $newEventType->schemas,
            $versionConverter
        );
    }

    public function getByEventClass(string $eventClass): EventType
    {
        foreach ($this->eventTypes as $eventType) {
            if ($eventType->eventClass === $eventClass) {
                return $eventType;
            }
        }
        throw EventTypeDoesNotExistException::forEventClass($eventClass);
    }

    public function getByEventType(string $eventType): EventType
    {
        foreach ($this->eventTypes as $existingEventType) {
            if ($existingEventType->toString() === $eventType) {
                return $existingEventType;
            }
        }
        throw EventTypeDoesNotExistException::forEventType($eventType);
    }
}
