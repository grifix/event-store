<?php
declare(strict_types=1);

namespace Grifix\EventStore\EventType\Repository\Exceptions;

final class EventTypeDoesNotExistException extends \Exception
{

    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function forEventClass(string $eventClass): self
    {
        return new self(sprintf('Event type for event class [%s] does not exist!', $eventClass));
    }

    public static function forEventType(string $eventType): self
    {
        return new self(sprintf('Event type  [%s] does not exist!', $eventType));
    }
}
