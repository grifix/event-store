<?php

declare(strict_types=1);

namespace Grifix\EventStore\EventType;

use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;

final class EventType
{
    public readonly int $version;

    /**
     * @param Schema[] $schemas
     */
    public function __construct(
        public readonly string $streamTypeName,
        public readonly string $name,
        public readonly string $eventClass,
        public readonly array $schemas,
        public readonly ?string $versionConverterClass = null
    ) {
        $this->version = count($schemas);
    }

    public function toString(): string
    {
        return $this->streamTypeName . '.' . $this->name;
    }
}
