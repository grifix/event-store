<?php

declare(strict_types=1);

namespace Grifix\EventStore\Workers;

use Doctrine\DBAL\Connection;
use Grifix\BigInt\BigInt;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\Event\Repository\EventRepositoryInterface;
use Grifix\EventStore\EventType\Repository\EventTypeRepositoryInterface;
use Grifix\EventStore\Subscription\Exceptions\InvalidEventNumberException;
use Grifix\EventStore\Subscription\Exceptions\SubscriptionIsNotActiveException;
use Grifix\EventStore\Subscription\Repository\SubscriptionRepositoryInterface;
use Grifix\EventStore\Subscription\Subscription;
use Grifix\EventStore\Subscription\SubscriptionFactory;
use Grifix\EventStore\SubscriptionType\Repository\SubscriptionTypeRepositoryInterface;
use Grifix\EventStore\SubscriptionType\SubscriptionType;
use Grifix\EventStore\Workers\Exceptions\InvalidEventClassException;
use Psr\Log\LoggerInterface;
use Throwable;

final class EventProcessor
{

    public function __construct(
        private readonly Connection $connection,
        private readonly SubscriptionRepositoryInterface $subscriptionRepository,
        private readonly SubscriptionTypeRepositoryInterface $subscriptionTypeRepository,
        private readonly SubscriptionFactory $subscriptionFactory,
        private readonly EventRepositoryInterface $eventRepository,
        private readonly LoggerInterface $logger,
        private readonly EventTypeRepositoryInterface $eventTypeRepository
    ) {
    }

    public function __invoke(EventEnvelope $envelope): void
    {
        $this->connection->connect();
        $this->assertEventClass($envelope);
        foreach ($this->subscriptionTypeRepository->findByStreamType($envelope->streamTypeName) as $subscriptionType) {
            $this->processSubscription($envelope, $subscriptionType);
        }
        if (!$this->connection->isTransactionActive()) {
            $this->connection->close();
        }
    }

    private function assertEventClass(EventEnvelope $envelope): void
    {
        $eventType = $this->eventTypeRepository->getByEventType($envelope->getEventType());
        if ($eventType->eventClass !== $envelope->event::class) {
            throw new InvalidEventClassException(
                $eventType->toString(),
                $eventType->eventClass,
                $envelope->event::class
            );
        }
    }

    private function processSubscription(EventEnvelope $envelope, SubscriptionType $subscriptionType): void
    {
        if (!$this->subscriptionRepository->has($subscriptionType->toString(), $envelope->streamId->toString())) {
            if (!$subscriptionType->isStartingEventType($envelope->getEventType())) {
                return;
            }
            $this->createNewSubscription($subscriptionType, $envelope);
        }

        $this->connection->beginTransaction();
        try {
            $this->processEvent($envelope, $subscriptionType);
            $this->subscriptionRepository->flush();
        } catch (Throwable $exception) {
            if ($this->isExceptionCritical($exception)) {
                $this->handleCriticalException($exception);
            } else {
                $this->handleNonCriticalException($exception);
            }
        } finally {
            $this->subscriptionRepository->clear();
        }

        if ($this->connection->isTransactionActive()) {
            $this->connection->commit();
        }
    }

    private function isExceptionCritical(Throwable $exception): bool
    {
        return false === str_contains($exception::class, "\\Domain\\");
    }

    private function handleCriticalException(Throwable $exception): void
    {
        $this->logger->critical(
            $exception->getMessage(),
            ['trace' => $exception->getTraceAsString()]
        );
        if ($this->connection->isTransactionActive()) {
            $this->connection->rollBack();
        }
    }

    private function handleNonCriticalException(Throwable $exception): void
    {
        $this->logger->error(
            $exception->getMessage(),
        );
    }

    private function processEvent(EventEnvelope $envelope, SubscriptionType $subscriptionType): void
    {
        $subscription = $this->subscriptionRepository->get(
            $subscriptionType->toString(),
            $envelope->streamId->toString()
        );
        try {
            $subscription->processEvent($envelope);
        } catch (InvalidEventNumberException $exception) {
            $this->logger->notice($exception->getMessage());
            $this->processMissedEvents(BigInt::create($exception->expectedNumber), $subscription);
        } catch (SubscriptionIsNotActiveException $exception) {
            $this->logger->notice($exception->getMessage());
        }
    }

    private function processMissedEvents(BigInt $fromNumber, Subscription $subscription): void
    {
        foreach (
            $this->eventRepository->find(
                fromNumber: $fromNumber,
                streamId: $subscription->getStreamId(),
                limit: 1000
            ) as $envelope
        ) {
            $subscription->processEvent($envelope);
        }
    }


    private function createNewSubscription(SubscriptionType $subscriptionType, EventEnvelope $envelope): void
    {
        $this->connection->beginTransaction();
        $result = $this->subscriptionFactory->create($subscriptionType->toString(), $envelope);
        $this->subscriptionRepository->add($result);
        $this->subscriptionRepository->flush();
        $this->connection->commit();
    }
}
