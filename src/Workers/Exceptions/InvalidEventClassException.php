<?php

declare(strict_types=1);

namespace Grifix\EventStore\Workers\Exceptions;

final class InvalidEventClassException extends \Exception
{

    public function __construct(string $eventType, string $expectedEventClass, string $actualEventClass)
    {
        parent::__construct(
            sprintf(
                'Invalid event class [%s] for event type [%s], event class [%s] expected!',
                $actualEventClass,
                $eventType,
                $expectedEventClass
            )
        );
    }
}
