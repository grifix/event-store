<?php

declare(strict_types=1);

namespace Grifix\EventStore\DependencyProvider;

interface DependencyProviderInterface
{
    public function set(object $dependency): void;

    public function get(string $className): object;
}
