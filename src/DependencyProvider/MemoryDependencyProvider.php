<?php

declare(strict_types=1);

namespace Grifix\EventStore\DependencyProvider;

final class MemoryDependencyProvider implements DependencyProviderInterface
{
    private array $dependencies = [];

    public function set(object $dependency): void
    {
        $this->dependencies[$dependency::class] = $dependency;
    }

    public function get(string $className): object
    {
        return $this->dependencies[$className];
    }
}
