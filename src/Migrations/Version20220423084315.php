<?php

declare(strict_types=1);

namespace Grifix\EventStore\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220423084315 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->createSchema();
        $this->createEventsTable();
        $this->createSubscriptionsTable();
    }

    public function down(Schema $schema): void
    {
        $this->dropSubscriptionsTable();
        $this->dropEventsTable();
        $this->dropSchema();
    }

    private function createSchema(): void
    {
        $sql = <<<SQL
create schema grifix_event_store
SQL;
        $this->addSql($sql);
    }

    private function dropSchema(): void
    {
        $this->addSql('drop schema grifix_event_store');
    }

    private function createEventsTable(): void
    {
        $sql = <<<SQL
create table grifix_event_store.events
(
    id               uuid      not null
        constraint events_pk
            primary key,
    stream_id        uuid      not null,
    stream_type_name      varchar(255) not null,
    event_type_name             varchar(255) not null,
    number           bigint    not null,
    date_of_creation timestamp not null,
    date_of_publishing timestamp,
    event            jsonb     not null,
    constraint events_number_key
        unique (stream_id, stream_type_name, number)
);
SQL;
        $this->addSql($sql);
    }

    private function dropEventsTable(): void
    {
        $this->addSql('drop table grifix_event_store.events');
    }

    private function createSubscriptionsTable(): void
    {
        $sql = <<<SQL
create table grifix_event_store.subscriptions
(
    type                       varchar(255)     not null,
    stream_id                  uuid             not null,
    status                     varchar(55)      not null,
    last_received_event_number bigint default 0 not null,
    constraint subscriptions_pk
        primary key (stream_id, type)
);
SQL;
        $this->addSql($sql);

    }

    private function dropSubscriptionsTable(): void
    {
        $this->addSql('drop table grifix_event_store.subscriptions');
    }
}
