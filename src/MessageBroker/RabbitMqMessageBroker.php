<?php

declare(strict_types=1);

namespace Grifix\EventStore\MessageBroker;

use Grifix\BigInt\BigInt;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\Normalizers\UuidNormalizer;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\DateTimeImmutableNormalizer;
use Grifix\Normalizer\SchemaValidator\Repository\Schema\Schema;
use Grifix\Uuid\Uuid;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

final class RabbitMqMessageBroker implements MessageBrokerInterface
{
    private readonly AMQPChannel $channel;

    private const QUEUE = 'grifix_event_store.events';

    public function __construct(
        private readonly AMQPStreamConnection $connection,
        private readonly NormalizerInterface $normalizer
    ) {
        $this->channel = $this->connection->channel();

        if(!$this->normalizer->hasNormalizerWithObjectClass(Uuid::class)){
            $this->normalizer->registerCustomObjectNormalizer(
                'grifix.Uuid',
                new UuidNormalizer(),
                [
                    Schema::create()
                        ->withStringProperty('value')
                ],
            );
        }

        if(!$this->normalizer->hasNormalizerWithObjectClass(\DateTimeImmutable::class)){
            $this->normalizer->registerCustomObjectNormalizer(
                'dateTimeImmutable',
                new DateTimeImmutableNormalizer(),
                [
                    Schema::create()
                        ->withStringProperty('value')
                ],
            );
        }

        if(!$this->normalizer->hasNormalizerWithObjectClass(BigInt::class)){
            $this->normalizer->registerDefaultObjectNormalizer(
                'grifix.BigInt',
                BigInt::class,
                [
                    Schema::create()
                    ->withNumberProperty('value')
                ],
            );
        }

        $this->normalizer->registerDefaultObjectNormalizer(
            'grifix.event_store.envelope',
            EventEnvelope::class,
            [
                Schema::create()
                    ->withObjectProperty('id', ['grifix.Uuid'])
                    ->withStringProperty('streamTypeName')
                    ->withObjectProperty('streamId', ['grifix.Uuid'])
                    ->withObjectProperty('number', ['grifix.BigInt'])
                    ->withObjectProperty('dateOfCreation', ['DateTimeImmutable'])
                    ->withStringProperty('eventTypeName')
                    ->withMixedObjectProperty('event')
                    ->withObjectProperty('dateOfPublishing', ['DateTimeImmutable'], true)
            ],
        );
    }

    public function send(object $message): void
    {
        $this->createQueue();
        $msg = new AMQPMessage(
            json_encode($this->normalizer->normalize($message)),
            [
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
            ]
        );
        $this->channel->basic_publish($msg, '', self::QUEUE);
    }

    public function startConsumer(callable $consumer): void
    {
        $this->createQueue();
        $this->channel->basic_consume(
            queue: self::QUEUE,

            callback: function (AMQPMessage $msg) use ($consumer) {
                /** @var EventEnvelope $envelope */
                $envelope = $this->normalizer->denormalize(json_decode($msg->body, true));
                $consumer($envelope);
                $msg->ack();
            }
        );
        while ($this->channel->is_open()) {
            $this->channel->wait();
        }
        $this->close();
    }

    private function createQueue(): void
    {
        $this->channel->queue_declare(
            queue: self::QUEUE,
            durable: true,
        );
    }

    private function close(): void
    {
        $this->channel->close();
        $this->connection->close();
    }

    public function __destruct()
    {
        $this->close();
    }
}
