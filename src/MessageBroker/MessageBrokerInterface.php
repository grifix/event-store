<?php
declare(strict_types=1);

namespace Grifix\EventStore\MessageBroker;

interface MessageBrokerInterface
{
    public function send(object $message): void;

    public function startConsumer(callable $consumer): void;
}
