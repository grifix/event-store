<?php
declare(strict_types=1);

namespace Grifix\EventStore\SubscriptionType\Exceptions;

final class InitialEventDefinitionDoesNotExistException extends \Exception
{
    public function __construct(string $eventType)
    {
        parent::__construct(sprintf('Initial event definition for event type [%d] does not exist.', $eventType));
    }
}
