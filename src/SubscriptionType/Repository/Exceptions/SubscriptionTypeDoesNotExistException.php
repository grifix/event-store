<?php
declare(strict_types=1);

namespace Grifix\EventStore\SubscriptionType\Repository\Exceptions;

use Exception;

final class SubscriptionTypeDoesNotExistException extends Exception
{

    public function __construct(string $name)
    {
        parent::__construct(sprintf('Subscription type [%s] does not exist', $name));
    }
}
