<?php
declare(strict_types=1);

namespace Grifix\EventStore\SubscriptionType\Repository;

use Grifix\EventStore\SubscriptionType\Repository\Exceptions\SubscriptionTypeDoesNotExistException;
use Grifix\EventStore\SubscriptionType\SubscriptionType;

interface SubscriptionTypeRepositoryInterface
{
    public function add(SubscriptionType $newSubscriptionType): void;

    public function findByStreamType(string $streamType): array;

    /**
     * @throws SubscriptionTypeDoesNotExistException
     */
    public function getByName(string $name): SubscriptionType;
}
