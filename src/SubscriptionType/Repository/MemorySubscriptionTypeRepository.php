<?php
declare(strict_types=1);

namespace Grifix\EventStore\SubscriptionType\Repository;

use Grifix\EventStore\SubscriptionType\Repository\Exceptions\SubscriptionTypeAlreadyExistsException;
use Grifix\EventStore\SubscriptionType\Repository\Exceptions\SubscriptionTypeDoesNotExistException;
use Grifix\EventStore\SubscriptionType\SubscriptionType;

final class MemorySubscriptionTypeRepository implements SubscriptionTypeRepositoryInterface
{
    /**
     * @var SubscriptionType[]
     */
    private array $subscriptionTypes = [];


    public function add(SubscriptionType $newSubscriptionType): void
    {
        foreach ($this->subscriptionTypes as $subscriptionType) {
            if ($subscriptionType->name === $newSubscriptionType->name) {
                throw new SubscriptionTypeAlreadyExistsException($newSubscriptionType->name);
            }
        }
        $this->subscriptionTypes[] = $newSubscriptionType;
    }

    public function findByStreamType(string $streamType): array
    {
        $result = [];
        foreach ($this->subscriptionTypes as $subscriptionType) {
            if ($subscriptionType->streamType->hasSameName($streamType)) {
                $result[] = $subscriptionType;
            }
        }
        return $result;
    }

    /**
     * @throws SubscriptionTypeDoesNotExistException
     */
    public function getByName(string $name): SubscriptionType
    {
        foreach ($this->subscriptionTypes as $subscriptionType) {
            if ($subscriptionType->name === $name) {
                return $subscriptionType;
            }
        }
        throw new SubscriptionTypeDoesNotExistException($name);
    }
}
